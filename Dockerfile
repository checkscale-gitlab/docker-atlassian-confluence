FROM java:openjdk-8u72-jdk
MAINTAINER Michal Wolonkiewicz http://blog.wolonkiewi.cz
 
 
# Use the default unprivileged account. This could be considered bad practice
# on systems where multiple processes end up being executed by 'daemon' but
# here we only ever run one process anyway.
ENV RUN_USER            daemon
ENV RUN_GROUP           daemon
 
 
ENV CONFLUENCE_HOME /var/atlassian/application-data/confluence
 
 
# Install Atlassian Confluence to the following location
ENV CONFLUENCE_INSTALL_DIR /opt/atlassian/confluence
 
 
ENV CONFLUENCE_VERSION 5.8.18
ENV DOWNLOAD_URL https://www.atlassian.com/software/confluence/downloads/binary/atlassian-confluence-${CONFLUENCE_VERSION}.tar.gz
 
 
RUN mkdir -p                             ${CONFLUENCE_INSTALL_DIR} \
    && curl -L --silent                  ${DOWNLOAD_URL} | tar -xz --strip=1 -C "$CONFLUENCE_INSTALL_DIR" \
    && echo -e "\n\nconfluence.home=${CONFLUENCE_HOME}" >> $CONFLUENCE_INSTALL_DIR/confluence/WEB-INF/classes/confluence-init.properties
 
 
# Update JVM memory settings
RUN sed -i -e"s/\-Xmx1024m/\-Xmx12288m/" $CONFLUENCE_INSTALL_DIR/bin/setenv.sh \
 && sed -i -e"s/\-Xms1024m/\-Xms3076m/" $CONFLUENCE_INSTALL_DIR/bin/setenv.sh
 
 
# Add Oracle JDBC driver https://confluence.atlassian.com/conf58/database-jdbc-drivers-771892795.html
# Use ojdbc6.jar for Oracle 11.1 and 11.2
# Use ojdbc7.jar for Oracle 12c
# This need to be downloaded manually from http://www.oracle.com/technetwork/database/features/jdbc/index-091264.html
ADD ojdbc6.jar ${CONFLUENCE_INSTALL_DIR}/confluence/WEB-INF/lib
 
 
# Fix permissions
RUN chmod -R 700                      ${CONFLUENCE_INSTALL_DIR}/logs               \
    && chmod -R 700                      ${CONFLUENCE_INSTALL_DIR}/temp               \
    && chmod -R 700                      ${CONFLUENCE_INSTALL_DIR}/work               \
    && chown -R ${RUN_USER}:${RUN_GROUP} ${CONFLUENCE_INSTALL_DIR}/
 
 
USER ${RUN_USER}:${RUN_GROUP}
 
VOLUME ["${CONFLUENCE_HOME}"]
 
# HTTP Port
EXPOSE 8090
 
WORKDIR $CONFLUENCE_INSTALL_DIR
 
# Run in foreground. This makes Tomcat/Catalina output easy to view via 'docker logs <container-name>' command.
CMD ["./bin/start-confluence.sh", "-fg"]
 
